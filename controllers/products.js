const database = require("../utils");
const nodemailer = require("nodemailer");

// create reusable transporter object using the default SMTP transport
let transporter = nodemailer.createTransport({
    host: "mail.shopnshare.co.uk",
    port: 465,
    secure: true, // true for 465, false for other ports
    auth: {
        user: 'info@shopnshare.co.uk', // generated ethereal user
        pass: 'sWac?D;e[!R,' // generated ethereal password
    }
});

exports.sendEmail = async (req, res) => {
    if (!req.body.email && !req.body.to && !req.body.from) {
        return res.status(404).json({'status_code': 404, 'error': "Requirements Missing."});
    }
    console.log("");
    let info = await transporter.sendMail({
        from: 'info@shopnshare.co.uk', // sender address
        to: req.body.to.trim(), // list of receivers
        subject: "Grocery List for " + req.body.from.trim(), // Subject line
        text: req.body.email // plain text body
    });
    if (info.accepted.length > 0) {
        return res.status(200).json({'status_code': 200, 'message': "Email Sent"});
    }
    res.status(404).json({'status_code': 404, 'message': "Email Sending Failed"});
};

exports.getProducts = (req, res) => {
    try {
        	let letters = /^[0-9a-zA-Z ]+$/;
	    if (req.query.store.match(letters) && req.query.title.match(letters)) {
            if (!req.query.store) {
                return res.status(404).json({'status_code': 404, 'error': "Select a store"});
            }
            if (!req.query.title) {
                return res.status(404).json({'status_code': 404, 'error': "Enter a title"});
            }
            // making it full text search and removing availablity
            const query = "select id, title,price,store,pricePerWeight,imageUrl,category, MATCH(title) AGAINST('" + req.query.title.trim() + "' IN BOOLEAN MODE) as relevance from products where store = '" + req.query.store.trim() + "' and MATCH(title) AGAINST('" + req.query.title + "' IN BOOLEAN MODE) ORDER BY relevance DESC LIMIT 150";
            // const query = "select id, title, price, store, pricePerWeight, imageUrl, category from products where store = '" + req.query.store.trim() + "' and title like '" + req.query.title + "%' ORDER BY title asc LIMIT 100";
            database.executeQuery(res, "", query);
        } else {
            return res.status(404).json({'status_code': 404, 'error': "Special characters are not allowed."});
        }
    } catch (e) {
        console.log(e);
        const response = {'status_code': 500, 'error': "Internal Server Error"};
        res.status(500).json(response);
    }
};

exports.getInTouch = async (req, res) => {
    if (!req.body.message && !req.body.name && !req.body.email) {
        return res.status(404).json({'status_code': 404, 'error': "Requirements Missing."});
    }
    console.log("");
    let info = await transporter.sendMail({
        from: 'info@shopnshare.co.uk', // sender address
        to: 'info@shopnshare.co.uk', // list of receivers
        subject: "Suggestion", // Subject line
        text: "Hi,\n\n the following store was requested. \n\n " +
            "Name: " + req.body.name.trim() + "\n\n" +
            "Email: " +req.body.email.trim() + "\n\n" +
            "Message: " +req.body.message.trim() + "\n\n"  // plain text body
    });
    if (info.accepted.length > 0) {
        return res.status(200).json({'status_code': 200, 'message': "Email Sent"});
    }
    res.status(404).json({'status_code': 404, 'message': "Email Sending Failed"});
};
