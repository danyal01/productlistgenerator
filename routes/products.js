const express = require("express");
const router = express.Router();
const ProductsController = require("../controllers/products");

router.post("/email", ProductsController.sendEmail);
router.get("/products", ProductsController.getProducts);
router.post("/getintouch", ProductsController.getInTouch);

module.exports = router;
