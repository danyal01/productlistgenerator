require('dotenv').config();
const express = require("express");
const path = require('path');
const bodyParser = require("body-parser");
const cors = require("cors");
const routes = require("./routes/routes");

const config = express();
config.use("/", express.static(path.join(__dirname, 'public')));

// Body Parser Middleware
config.use(bodyParser.json({limit:'100mb'}));
config.use(bodyParser.urlencoded({limit: '100mb', extended: true}));
config.use(cors());

config.use('/api', routes);

module.exports = config;
