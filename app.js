require('dotenv').config();
const app = require("./config");
const database = require("./utils");
const fs = require('fs');
const http = require('http');

const port = process.env.PORT || 8000;

app.set('port', port);
const httpsServer = http.createServer(app);

httpsServer.listen(port, () => {
	console.log('HTTPS Server running on port ', port);
});

database.con.connect(function (err) {
    if (err) {
        console.log("Error while connecting database :- " + err);
    } else {
        console.log("Database Connected");
    }
});
