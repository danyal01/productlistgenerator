let basket = [];

function createShareMessage() {
    let shareText = "";
    let data = [];
    let total = 0;
    if (basket.length > 0) {
        const symbol = basket[0].price.replace(/[0-9.]/gi, "");
        basket.map(item => {
            total += item.subTotal;
            data.push([
                item.store + " | ",
                "*" + item.title.replace(/&/g, 'and') + "* | ",
                item.price + " | ",
                item.qty + " item(s) "
            ]);
        });
    }

    shareText = 'Hi, \n\nIf you are going to the shop, please could you purchase me the following items. The total cost should be about *£' + total + '*\n\n';
    for (row of data) {
        shareText += row.toString().replace(/,/g, '') + "\n\n";
    }
    shareText += "I used www.shopnshare.co.uk to make this list. Please STAY SAFE";
    return shareText;
};

async function sharePDF() {
    var doc = new jsPDF();
    var splitTitle = doc.splitTextToSize(await createShareMessage(), 180);
    doc.text(20, 20, splitTitle);
    doc.save('document.pdf');
}

async function shareEmail() {
    let body = {
        email: await createShareMessage(),
        to: "danyal20098@gmail.com",
        from: "Senders Name"
    };
    const url = `/api/email`;
    const response = await fetch(url, {
        method: "POST",
        headers: {
            'Content-Type': 'application/json;charset=utf-8'
        },
        body: JSON.stringify(body)
    });
    const result = await response.json();
    if (result.status_code === 200) {
        showToast("List Emailed");
    } else {
        showToast("Error Occurred");
    }
}

async function shareWhatsapp() {
    let shareText = await createShareMessage();
    document.getElementById("shareWhatsapp").href = "whatsapp://send?text=" + encodeURI(shareText) + "";
    return false;
}

function populateBasket(symbol) {
    let html = "";
    if (basket.length > 0) {
        html = `
        <h4 class="title">3. Finalize your products</h4>
        <table width="100%" class="table table-striped table-borderless table-hover table-sm display responsive nowrap">
            <thead class="basket-thead">
                <tr class="table-color text-white">
                    <th>Item</th>
                    <th>Price</th>
                    <th style="min-width: 130px;width: 130px">Quantity</th>
                    <th>Sub Total</th>
                    <th>Store</th>
                    <th>Category</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody class="basket-tbody">`;
        let total = 0;
        basket.map(product => {
            total += product.subTotal;
            html += `<tr data-id="${product.id}">
                    <td class="flex-center" style=" font-weight:700">
                        <img class="prod-img" src="${product.imageUrl}" alt=""/>
                        ${product.title}
                    </td>
                    <td>${product.price}</td>
                    <td>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <button class="btn btn-secondary btn-sm btn-sub basket plus-minus">
                                    <i class="fa fa-minus"></i>        
                                </button>
                            </div>
                            <input type="text" class="form-control text-center form-control-sm qty" 
                                   value="${product.qty}" readonly>
                            <div class="input-group-append">
                                <button class="btn btn-secondary btn-sm btn-add basket plus-minus">
                                    <i class="fa fa-plus"></i>
                                </button>
                            </div>
                        </div>
                    </td>
                    <td>${symbol}${product.subTotal.toFixed(2)}</td>
                    <td>${product.store}</td>
                    <td>${product.category}</td>
                    <td>
                        <button type="button" class="btn btn-remove btn-danger btn-sm"><i class="fa fa-trash"></i></button>
                    </td>
                </tr>`;
        });
        if (total > 0) {
            html += `<tr class="total-class">
            <td class="text-right" colspan="6">Total</td>
            <td>${symbol}${total.toFixed(2)}</td>
        </tr>`;
        }
        html += `</tbody></table>`;
        $("#basket-actions").removeClass("d-none");
    } else {
        $("#basket-status").html('<div class="alert alert-danger mb-0">Your basket is empty</div>');
        $("#basket-actions").addClass("d-none");
    }
    // Save to localStorage
    localStorage.setItem("basket", JSON.stringify(basket));
    $("#basket-container").html(html);
    shareWhatsapp();
}

function showToast(message) {
    var x = document.getElementById("snackbar");
    x.className = "show";
    x.innerHTML = message;
    setTimeout(function () {
        x.className = x.className.replace("show", "");
    }, 3000);
}

$(document).ready(function () {
    let dtProducts = $("#dt-products").DataTable({
        columnDefs: [
            {responsivePriority: 1, targets: 0},
            {responsivePriority: 2, targets: 1},
            {responsivePriority: 3, targets: 5},
            {responsivePriority: 4, targets: 3},
            {responsivePriority: 5, targets: 4},
            {responsivePriority: 6, targets: 2},
        ],
        responsive: {
            details: {
                renderer: function (api, rowIdx, columns) {
                    var data = $.map(columns, function (col, i) {
                        return col.hidden ?
                            '<tr data-dt-row="' + col.rowIndex + '" data-dt-column="' + col.columnIndex + '">' +
                            '<th>' + col.title + '</th> ' +
                            '<td>' + col.data + '</td>' +
                            '</tr>' :
                            '';
                    }).join('');
                    console.log(columns);
                    return data ?
                        $('<table style="width: 98%; margin: 0.5em"/>').append(data) :
                        false;
                }
            }
        }
    });
    const b = localStorage.getItem("basket");
    if (b) {
        basket = JSON.parse(b);
    }
    let symbol = "";
    if (basket.length > 0) {
        symbol = basket[0].price.replace(/[0-9.]/gi, "");
    }
    populateBasket(symbol);

    $("#btn-search").on('click', function (e) {
        const searchVal = $("#searchField").val();
        if (searchVal) {
            loadProducts(searchVal);
        } else {
            alert("Invalid Input")
        }
    });

    async function loadProducts(input) {
        $("#products").html("");
        const store = $("#store").val();
        const url = `/api/products?store=${store}&title=${input}`;
        const response = await fetch(url, {
            method: "get"
        });
        const result = await response.json();
        if (result.status_code === 200) {
            let html = "";
            const products = result.data.files;
            for (let product of products) {
                const item = JSON.stringify(product);
                html += `<tr data-item='${item}'>
                        <td class="flex-center">
                            <img class="prod-img" src="${product.imageUrl}" alt=""/>
                            <span class="item-title">${product.title}</span>
                        </td>
                        <td>${product.price}</td>
                        <td>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <button class="btn btn-secondary btn-sm btn-sub plus-minus">
                                        <i class="fa fa-minus"></i>        
                                    </button>
                                </div>
                                <input type="text" class="form-control text-center form-control-sm qty" 
                                   value="1" readonly>
                                <div class="input-group-append">
                                    <button class="btn btn-secondary btn-sm btn-add plus-minus">
                                        <i class="fa fa-plus"></i>        
                                    </button>
                                </div>
                            </div>
                        </td>
                        <td>${product.store}</td>
                        <td>${product.category}</td>
                        <td style="min-width: 120px">
                            <button type="button" class="btn btn-add-to-cart table-color text-white btn-sm btn-add-cart"><i class="fa fa-plus" aria-hidden="true"></i></button>
                        </td>
                    </tr>`;
            }
            dtProducts.clear().destroy();
            $('#products').html(html);
            setTimeout(function () {
                dtProducts = $("#dt-products").DataTable({
                    "pagingType": "simple_numbers"
                });
            }, 200);
        } else {
            console.log(result);
            showToast(result.error);
        }
    }
});

$(document).on("click", ".btn-add-to-cart", function (e) {
    let tr = $(this).closest("tr");
    if (!tr.data("item")) {
        tr = tr.next('tr');
        console.log(tr);
    }
    let item = tr.attr("data-item");
    item = JSON.parse(item);
    let qty = tr.find(".qty").val();
    qty = Number.parseInt(qty, 10);
    if (qty > 0) {
        const index = _.findIndex(basket, {id: item.id});
        let price = item.price.replace(/[^0-9.]/gi, "");
        const symbol = item.price.replace(/[0-9.]/gi, "");
        price = Number.parseFloat(price);
        if (index !== -1) {
            basket[index].qty += qty;
            basket[index].subTotal = basket[index].qty * price;
        } else {
            item.qty = qty;
            item.subTotal = qty * price;
            basket.push(item);
        }
        populateBasket(symbol);
        showToast("Item Added to Basket");
    }
});

$(document).on("click", ".btn-add, .btn-sub", function (e) {
    let qty = $(this).closest("tr").find(".qty").val();
    qty = Number.parseInt(qty, 10);
    if ($(e.target).hasClass("btn-add")) {
        qty = qty + 1;
    } else {
        if (qty > 1) {
            qty = qty - 1;
        }
    }
    $(this).closest("tr").find(".qty").val(qty);
    if ($(e.target).hasClass("basket")) {
        const id = $(this).closest("tr").attr("data-id");
        const index = _.findIndex(basket, {id: Number(id)});
        if (index !== -1) {
            basket[index].qty = qty;
            let price = basket[index].price.replace(/[^0-9.]/gi, "");
            const symbol = basket[index].price.replace(/[0-9.]/gi, "");
            price = Number.parseFloat(price);
            basket[index].subTotal = qty * price;
            populateBasket(symbol);
        }
    }
});

$(document).on("click", ".btn-remove", function () {
    const id = $(this).closest("tr").attr("data-id");
    const index = _.findIndex(basket, {id: Number(id)});
    if (index !== -1) {
        basket.splice(index, 1);
        let symbol = "";
        if (basket.length > 0) {
            symbol = basket[0].price.replace(/[0-9.]/gi, "");
        }
        populateBasket(symbol);
        showToast("Item removed from basket");
    }
});

$(document).on("click", ".btn-clear-basket", function () {
    basket = [];
    populateBasket("");
    showToast("Basket Empty");
});
//search on enter
var input_search = $("#searchField");
input_search.keyup(function(event) {
    if (event.keyCode === 13) {
        $("#btn-search").click();
    }
});
//media query
function myFunction(x) {
  if (x.matches) { // If media query matches
     $("#searchField").attr("placeholder", "Search here....");
  } else {
  $("#searchField").attr("placeholder", "What are you looking for?");
  }
}

var x = window.matchMedia("(max-width: 776px)");
myFunction(x) // Call listener function at run time
x.addListener(myFunction)
