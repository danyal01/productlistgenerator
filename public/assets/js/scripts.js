let basket = [];

function createShareMessage() {
    let shareText = "";
    let data = [];
    let total = 0;
    let _itemText = 'item';
    if (basket.length > 0) {
        const symbol = basket[0].price.replace(/[0-9.]/gi, "");
        basket.map(item => {
            _itemText = item.qty === 1 ? ' item' : ' items';
            total += item.subTotal;
            data.push([
                item.store + " | ",
                "*" + item.title.replace(/&/g, 'and') + "* | ",
                item.price + " | ",
                item.qty + _itemText
            ]);
        });
    }

    shareText = 'Hi, \n\nIf you are going to the shop, please could you purchase me the following items. The total cost should be about *£' + total.toFixed(2) + '*\n\n';
    for (row of data) {
        shareText += row.toString().replace(/,/g, '') + "\n\n";
    }
    shareText += "I used www.shopnshare.co.uk to make this list. Please STAY SAFE";
    return shareText;
};

async function sharePDF() {
    var doc = new jsPDF();
    var splitTitle = doc.splitTextToSize(await createShareMessage(), 180);
    doc.text(20, 20, splitTitle);
    doc.save('Shop N Share List.pdf');
}

async function shareEmail() {
    let body = {
        email: await createShareMessage(),
        to: $("#toEmail").val(),
        from: $("#fromEmail").val()
    };
    const url = `/api/email`;
    const response = await fetch(url, {
        method: "POST",
        headers: {
            'Content-Type': 'application/json;charset=utf-8'
        },
        body: JSON.stringify(body)
    });
    const result = await response.json();
    if (result.status_code === 200) {
        showToast("Email sent");
    } else {
        showToast("Error Occurred");
    }
}

async function shareWhatsapp() {
    let shareText = await createShareMessage();
    document.getElementById("shareWhatsapp").href = "whatsapp://send?text=" + encodeURI(shareText) + "";
    return false;
}

async function getInTouch() {
    let body = {
		message: $("#fieldMessage").val(),
        name: $("#fieldName").val(),
        email: $("#fieldEmail").val(),
    };
    const url = `/api/getintouch`;
    const response = await fetch(url, {
        method: "POST",
        headers: {
            'Content-Type': 'application/json;charset=utf-8'
        },
        body: JSON.stringify(body)
    });
    const result = await response.json();
    if (result.status_code === 200) {
        showToast("Email sent");
		$("#fieldMessage").val('');
		$("#fieldName").val('');
		$("#fieldEmail").val('');
		$('#contactModal').modal('toggle');
    } else {
        showToast("Error Occurred");
		$("#fieldMessage").val('');
		$("#fieldName").val('');
		$("#fieldEmail").val('');
		$('#contactModal').modal('toggle');
    }
}

function populateBasket(symbol) {
    let html = "";
    if (basket.length > 0) {
        html = `
        <div class="col-lg-10 mr-auto p-0"><h1 class="p-2" style="text-align:left;margin-top:30px;padding-left:0 !important;">Basket</h1></div>
        <div class="col-lg-10 p-4 bg-white rounded shadow-sm mr-auto mb-5">
		<button class="btn btn-danger btn-clear-basket btn-sm on-small-device">
				<i class="fa fa-remove"></i> <span>Clear Basket</span></button>
        <div class="table-responsive">
            <table class="table">
              <thead>
                <tr>
                  <th scope="col" class="border-0 bg-light">
                    <div class="p-2 px-3 text-uppercase">Item</div>
                  </th>
                  <th scope="col" class="border-0 bg-light d-none d-md-block d-lg-block">
                    <div class="py-2 text-uppercase">Price</div>
                  </th>
				  <th scope="col" class="border-0 bg-light d-none d-md-block d-lg-block">
                    <div class="py-2 text-uppercase">Qty</div>
                  </th>
                  <th scope="col" class="border-0 bg-light d-none d-md-block d-lg-block">
                    <div class="py-2 text-uppercase">Sub Total</div>
                  </th>
                  <th scope="col" class="border-0 bg-light">
                    <div class="py-2 text-uppercase">Remove</div>
                  </th>
                </tr>
              </thead>
              <tbody>`;
        let total = 0;
        basket.map(product => {
            total += product.subTotal;
            html += `<tr data-id="${product.id}">
                  <th scope="row" class="border-0">
                    <div class="p-2">
                      <img src="${product.imageUrl}" alt="" width="40" class="img-fluid rounded shadow-sm">
                      <div class="ml-1 d-inline-block align-middle">
                        <h5 class="mb-0"> ${product.title}</h5><span class="text-muted font-weight-normal font-italic dsp-none">Category:  ${product.category}	
</span><span class="text-muted font-weight-normal font-italic dsp-none-lg">Price: ${product.price} | Qty: ${product.qty}</span>
                      </div>
                    </div>
                  </th>
                  <td class="border-0 align-middle d-none d-md-block d-lg-block"><strong>${product.price}</strong></td>
				  <td class="border-0 align-middle d-none d-md-block d-lg-block"><strong>${product.qty}<input type="hidden" class="form-control text-center form-control-sm qty" 
                                   value="${product.qty}" readonly></strong></td>
                  <td class="border-0 align-middle d-none d-md-block d-lg-block"><strong>${symbol}${product.subTotal.toFixed(2)}</strong></td>
                  <td class="border-0 align-middle"><button type="button" class="btn btn-remove btn-sm"><i class="fa fa-trash"></i></button></td>
                </tr>`;
        });
        html += `</tbody></table>`;
        if (total > 0) {
            html += `<ul class="list-unstyled mb-4">
              <li class="d-flex justify-content-between py-3 border-bottom"><strong class="text-muted"></li>
              <li class="d-flex justify-content-between pt-3"><strong class="text-muted"><h4>Total</h4></strong>
                <h4 class="font-weight-bold">${symbol}${total.toFixed(2)}</h4>
              </li>
			  <span class="text-muted font-weight-normal font-italic d-block">The total is estimated as a few costs appear as null from the various stores and prices also update on a daily basis	
</span>
<li class="d-flex justify-content-between py-3 border-bottom"><strong class="text-muted"></li>
            </ul>`;
        }
        html += `</div></div>`;
        $("#basket-actions").removeClass("d-none");
    } else {
        $("#basket-status").html('<div class="alert alert-danger mb-0">Your basket is empty</div>');
        $("#basket-actions").addClass("d-none");
    }
    // Save to localStorage
    localStorage.setItem("basket", JSON.stringify(basket));
    $("#basket-container").html(html);
    shareWhatsapp();
}

function showToast(message) {
    var x = document.getElementById("snackbar");
    x.className = "show";
    x.innerHTML = message;
    setTimeout(function () {
        x.className = x.className.replace("show", "");
    }, 3000);
}

$(document).ready(function () {
    let dtProducts = $("#dt-products1").DataTable({
        columnDefs: [
            {responsivePriority: 1, targets: 0},
            {responsivePriority: 2, targets: 1},
            {responsivePriority: 3, targets: 5},
            {responsivePriority: 4, targets: 3},
            {responsivePriority: 5, targets: 4},
            {responsivePriority: 6, targets: 2},
        ],
        responsive: {
            details: {
                renderer: function (api, rowIdx, columns) {
                    var data = $.map(columns, function (col, i) {
                        return col.hidden ?
                            '<tr data-dt-row="' + col.rowIndex + '" data-dt-column="' + col.columnIndex + '">' +
                            '<th>' + col.title + '</th> ' +
                            '<td>' + col.data + '</td>' +
                            '</tr>' :
                            '';
                    }).join('');
                    console.log(columns);
                    return data ?
                        $('<table style="width: 98%; margin: 0.5em"/>').append(data) :
                        false;
                }
            }
        }
    });
    const b = localStorage.getItem("basket");
    if (b) {
        basket = JSON.parse(b);
    }
    let symbol = "";
    if (basket.length > 0) {
        symbol = basket[0].price.replace(/[0-9.]/gi, "");
    }
    populateBasket(symbol);

    $("#btn-search").on('click', function (e) {
        const searchVal = $("#searchField").val();
        if (searchVal) {
            loadProducts(searchVal);
        } else {
            alert("Invalid Input")
        }
    });

    async function loadProducts(input) {
        $("#products").html("");
        const store = $("#store").val();
        const url = `/api/products?store=${store}&title=${input}`;
        const response = await fetch(url, {
            method: "get"
        });
        const result = await response.json();
        if (result.status_code === 200) {
            let html = "";
            const products = result.data.files;
            for (let product of products) {
                const item = JSON.stringify(product);
                html += `<tr data-item='${item}'>
                  <th scope="row" class="border-0">
                    <div class="p-2">
                      <img src="${product.imageUrl}" alt="" width="40" class="img-fluid rounded shadow-sm">
                      <div class="ml-1 d-inline-block align-middle">
                        <h5 class="mb-0"> ${product.title}</h5><span class="text-muted font-weight-normal font-italic dsp-none">Category:  ${product.category}	
</span><span class="text-muted font-weight-normal font-italic dsp-none-lg">Price: ${product.price}</span>
                      </div>
                    </div>
                  </th>
                  <td class="border-0 align-middle d-none d-md-block d-lg-block"><input type="hidden" class="form-control text-center form-control-sm qty" value="1" readonly><strong>${product.price}</strong></td>
                  <td class="border-0 align-middle d-none d-md-block d-lg-block"><strong>${product.store}</strong></td>
                  <td class="border-0 align-middle"><button type="button" class="btn btn-add-to-cart table-color text-white btn-sm btn-add-cart"><i class="fa fa-plus"></i>
</button></td>
                </tr>`;
            }
            dtProducts.clear().destroy();
            $('#products').html(html);
            $("html, body").animate({scrollTop: 0}, "slow");
            setTimeout(function () {
                dtProducts = $("#dt-products").DataTable({
                    "pagingType": "simple_numbers",
					"searching": false,
					"ordering": false,
					"lengthChange": false
                });
            }, 200);
        } else {
            console.log(result);
            showToast(result.error);
        }
    }
});

$(document).on("click", ".btn-add-to-cart", function (e) {
    let tr = $(this).closest("tr");
    if (!tr.data("item")) {
        tr = tr.next('tr');
        console.log(tr);
    }
    let item = tr.attr("data-item");
    item = JSON.parse(item);
    let qty = tr.find(".qty").val();
    qty = Number.parseInt(qty, 10);
    if (qty > 0) {
        const index = _.findIndex(basket, {id: item.id});
        let price = item.price.replace(/[^0-9.]/gi, "");
        const symbol = item.price.replace(/[0-9.]/gi, "");
        price = Number.parseFloat(price);
        if (index !== -1) {
            basket[index].qty += qty;
            basket[index].subTotal = basket[index].qty * price;
        } else {
            item.qty = qty;
            item.subTotal = qty * price;
            basket.push(item);
        }
        populateBasket(symbol);
        showToast("Item added to cart");
    }
});

$(document).on("click", ".btn-add, .btn-sub", function (e) {
    let qty = $(this).closest("tr").find(".qty").val();
    qty = Number.parseInt(qty, 10);
    if ($(e.target).hasClass("btn-add")) {
        qty = qty + 1;
    } else {
        if (qty > 1) {
            qty = qty - 1;
        }
    }
    $(this).closest("tr").find(".qty").val(qty);
    if ($(e.target).hasClass("basket")) {
        const id = $(this).closest("tr").attr("data-id");
        const index = _.findIndex(basket, {id: Number(id)});
        if (index !== -1) {
            basket[index].qty = qty;
            let price = basket[index].price.replace(/[^0-9.]/gi, "");
            const symbol = basket[index].price.replace(/[0-9.]/gi, "");
            price = Number.parseFloat(price);
            basket[index].subTotal = qty * price;
            populateBasket(symbol);
        }
    }
});

$(document).on("click", ".btn-remove", function () {
    const id = $(this).closest("tr").attr("data-id");
    const index = _.findIndex(basket, {id: Number(id)});
    if (index !== -1) {
        basket.splice(index, 1);
        let symbol = "";
        if (basket.length > 0) {
            symbol = basket[0].price.replace(/[0-9.]/gi, "");
        }
        populateBasket(symbol);
        showToast("Item removed from basket");
    }
});

$(document).on("click", ".btn-clear-basket", function () {
    basket = [];
    populateBasket("");
    showToast("Basket Empty");
});
//search on enter
var input_search = $("#searchField");
input_search.keyup(function (event) {
    if (event.keyCode === 13) {
        $("#btn-search").click();
    }
});
$(document).on("submit", "#frmgetIntouch", function (e) {
	e.preventDefault(); 
	getInTouch();
});

//media query
function myFunction(x) {
    if (x.matches) { // If media query matches
        $("#searchField").attr("placeholder", "Search here....");
    } else {
        $("#searchField").attr("placeholder", "What are you looking for?");
    }
}

$(document).on("click", ".basketIcon i", function () {
    $('html, body').animate({
        scrollTop: $("#basket-container").offset().top
    }, 2000);
});
var x = window.matchMedia("(max-width: 776px)");
myFunction(x) // Call listener function at run time
x.addListener(myFunction)
